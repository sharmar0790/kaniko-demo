# kaniko-demo


## Getting started  

Kaniko is a tool which help to build the docker image inside a pod or a running container in a kubernetes environemt.
Kaniko build the image in a rootless mode and build the image in a userspace.
It also does not require docker or docker daemon which makes it more secure than docker.

### Pre-requisite  
- git repo
- Kubernetes environment.
- Kubernetes secret to authneticate, so kaniko can push the image.

## Steps  
Building image with kaniko is quite simple.

### Create the secret  
Execute below command to create the secret
```shell
kubectl create secret docker-registry <NAME_OF_SECRET> \
    --docker-server=<>REGISTRY_URL \
    --docker-username=<REGISTRY_USERNAME> \
    --docker-password=<REGISTRY_PASSWORD_OR_TOKEN>\
    --docker-email=<REGISTRY_EMAIL>
```

### Creta the pod for `kaniko`  
Execute the below command to create the pod  
```pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: kaniko
spec:
  containers:
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug
    args:
    - "--context=<GIT_URL>"  # git URL from where kaniko will read the Docker file
    - "--destination=<DESTINATION_REGISTRY_URL>" # target registry where kaniko will push the image
    volumeMounts:
    - name: kaniko-secret
      mountPath: /kaniko/.docker
  restartPolicy: Never
  volumes:
  - name: kaniko-secret
    secret:
      secretName: dockercred
      items:
        - key: .dockerconfigjson
          path: config.json
```

Execute    
`$ kubectl apply -f pod.yaml`

